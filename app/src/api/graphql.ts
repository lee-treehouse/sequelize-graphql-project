import { ApolloClient, InMemoryCache } from '@apollo/client';

export const cache = new InMemoryCache();

const client = new ApolloClient({
  cache,
  uri: process.env.SERVICES_URI + '/graphql'
});

export default client;
