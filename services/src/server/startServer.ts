import { ApolloServer } from 'apollo-server-express';
import * as cors from 'cors';
import * as express from 'express';

import resolvers from '../graphql/resolvers/';
import typeDefs from '../graphql/typeDefs';
import accessEnv from '../helpers/accessEnv';

import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginLandingPageDisabled
} from 'apollo-server-core';

async function startApolloServer() {
  const PORT = accessEnv('PORT', 7001);

  const apolloServer = new ApolloServer({
    resolvers,
    typeDefs,
    plugins: [
      process.env.NODE_ENV === 'production'
        ? ApolloServerPluginLandingPageDisabled()
        : ApolloServerPluginLandingPageGraphQLPlayground()
    ]
  });

  const app = express();

  app.use(
    // using default options as specified here https://github.com/expressjs/cors
    // making them explicit (even though they are the default) since i'm overriding the tutorial
    cors({
      origin: '*',
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
      preflightContinue: false,
      optionsSuccessStatus: 204
    })
  );

  await apolloServer.start();

  apolloServer.applyMiddleware({ app, path: '/graphql' });

  app.listen(PORT, '0.0.0.0', () => {
    console.info(`GSD service listening on ${PORT}`);
  });
}
startApolloServer();
