import { Restaurant } from '#root/db/models';

const createRestaurantResolver = (
  context: any,
  { chefId, name }: { chefId: string; name: string }
) => {
  console.log('I hit the restaurant resolver');
  return Restaurant.create({ chefId, name });
};

export default createRestaurantResolver;
