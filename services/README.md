#GraphQL & Sequelize Demo Project 

## Getting the database set up 

You will need Docker in order to isntall a containerised MySQL dev environment. After getting docker, run the following command anywhere. 

```sh
docker run \
-p 0.0.0.0:7999:3306 \
-- name gsd-db \
-e MYSQL_ROOT_PASSWORD=password \
-e MYSQL_USER = gsd-dev \
-e MYSQL_PASSWORD=password \
-e MYSQL_DATABASE=gsd \
-d mysql:5.7
```

This will create a Dcoker isntance called `gsd-db`, running mySQL v5.7, with the root password being 'password'. It also creates a database called `gsd`, creates a user called `gsd-dev` (with password `password`), and assigns that user full permissions onto the `gsd` database. 

the above were old instructions and not working due to this error message which suggests image not available..

Unable to find image 'name:latest' locally
docker: Error response from daemon: pull access denied for name, repository does not exist or may require 'docker login': denied: requested access to the resource is denied.
See 'docker run --help'.

let's try making our own 

docker run --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d mysql:5.7.39

ok for this one i get 

❯ docker run --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d mysql:5.7.39
Unable to find image 'mysql:5.7.39' locally
5.7.39: Pulling from library/mysql
docker: no matching manifest for linux/arm64/v8 in the manifest list entries.
See 'docker run --help'.

ok now I think that arm means my mac1

what if i try adding this 
 --platform linux/x86_64

docker run --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d mysql:5.7.39 --platform linux/x86_64 
ok that didn't do it.. let me look at the docker hub mysql documentation again, found an arm link

docker run --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d arm64v8/mysql:8

bingo, that worked. ok let's see what it was missing - user, password, database 

docker run --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d arm64v8/mysql:8 -e MYSQL_USER = gsd-dev -e MYSQL_PASSWORD=password -e MYSQL_DATABASE=gsd

ok that doesn't work becasue container already exists /gsd-db 

I know I can use docker ps to list the containers.. ok i used ps to list then i used stop then rm

now i'll create again 

oops i forgot the port stuff. 

docker run -p 0.0.0.0:7999:3306  --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d arm64v8/mysql:8 -e MYSQL_USER = gsd-dev -e MYSQL_PASSWORD=password -e MYSQL_DATABASE=gsd

ok had various problems so lets see if we can do the user and password and database manually - this command works

docker run -p 0.0.0.0:7999:3306 --name gsd-db -e MYSQL_ROOT_PASSWORD=password -d arm64v8/mysql:8

ok, so i manually created a user gsd-dev/password but it's only going to be for this one instance/container so I will have to circle back. 
and that is using mysql qorkbench because i haven't been able to connect with sequel pro
now i will need to create the database gsd 

ok i executed the following
DROP DATABASE IF EXISTS `gsd`;
CREATE DATABASE `gsd`; 

now i'm gonna try again to see if i can connect with sequel pro. ok even with connection time 30 seconds, no go.. I will continue with workbench 

